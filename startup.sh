#!/bin/sh

SERVER_FOLDER=$HOME/MinecraftServer

if [ ! -L $SERVER_FOLDER ]; then
	ln -s $(pwd) $SERVER_FOLDER
fi
if [ ! -z $(tmux list-sessions | grep McStarter) ]; then
	tmux kill-session -t McStarter
fi
tmux new-session -d -s McStarter -c $(pwd) ./start.sh
