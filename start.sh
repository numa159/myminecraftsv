#!/bin/bash
# RAM="2048M"
RAM="4096M"
# RAM="6144M"
TMUX_NAME="McServer"
JAR_FILE="paper-98"
SERVER_FOLDER="$HOME/MinecraftServer"
SERVER_CMD="java -Xmx$RAM -Xms512M -server -XX:+UseG1GC -jar $JAR_FILE.jar nogui"
RESTART_TIME="06:00:00 AM"

function killServer() {
	if [[ ! $(tmux list-sessions | grep $TMUX_NAME) == "" ]]; then
		echo "Closing server"
		tmux send-keys -t $TMUX_NAME 'say Server restarting in 5s' Enter
		sleep 5
		tmux send-keys -t $TMUX_NAME 'stop' Enter
	fi
}

function restartServer() {
	killServer
	while [[ ! $(tmux list-sessions | grep $TMUX_NAME) == "" ]]
	do
		echo "Server not closed yet!"
		sleep .5
	done
	echo "Server closed"
	echo "Starting server"
	tmux new-session -d -s $TMUX_NAME -c $(pwd) $SERVER_CMD
	# tmux send-keys -t $TMUX_NAME "exec $SERVER_CMD" Enter
}

cd $SERVER_FOLDER
if [[ $1 != "nokill" ]]; then
	restartServer
fi
while true
do
	if  [ -z "$(tmux list-sessions | grep $TMUX_NAME)" ];
	#if  [[ $(date | cut -d ' ' -f 5,6) == $RESTART_TIME || $(tmux list-sessions | grep $TMUX_NAME) == "" ]];
	then
		restartServer
	fi
done
